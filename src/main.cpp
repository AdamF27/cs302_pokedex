//-----------------------------------------------------------------------------
// File: main.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 4/20/2019
// Project: CS302 Final Project - Pokedex
//
// Description: This is the main file for the graphical pokedex
//-----------------------------------------------------------------------------

#include <gtk/gtk.h>
#include <cstdio>
#include <iostream>
#include <limits>

#include "pokemon.hpp"
#include "pokedex.hpp"
#include "utility.hpp"
#include "buttons.hpp"

using namespace std;

// Utility Functions ----------------------------------------------------------

void usageCheck(const int &argc, char *argv[]);
void printUsage();

// Main Execution -------------------------------------------------------------
// Main window has to be created which takes a lot of code just defining the
// widgets that GTK uses
int main(int argc, char *argv[])
{
    // Check for correct arguments etc
    usageCheck(argc, argv);

    // Create Pokedex
    Pokedex dex;

    // Necessary info about dex
    vector<Pokemon*> mod = dex.getModDex();

    int MAX_INDEX = mod.size() - 1;

    // Setup initial variables for display
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    int img_1_index = -1;
    int img_2_index = 0;
    int img_3_index = 1;

    // Set image paths
    img_1_path = img_1_path + "invalid.png"; // Always the case initially

    if(!(img_2_index > MAX_INDEX)){

        img_2_path.append(to_string(mod[img_2_index]->getNum()));
        img_2_path.append(".png");
    }
    else{

        img_2_path = "sprites/invalid.png";
    }
    if(!(img_3_index > MAX_INDEX)){

        img_3_path.append(to_string(mod[img_3_index]->getNum()));
        img_3_path.append(".png");
    }
    else{

        img_3_path = "sprites/invalid.png";
    }

    // GUI Creation
	GtkBuilder *builder;

    // Windows
	GtkWidget *window;
    GtkWidget *sort_window;
    GtkWidget *search_window;
    GtkWidget *filter_window;

    // Secondary Windows
    GtkWidget *search_num_window;
    GtkWidget *search_name_window;
    GtkWidget *filter_ability_window;
    GtkWidget *filter_stats_window;
    GtkWidget *filter_type_window;

    // Non button widgets
    GtkWidget *img1;
    GtkWidget *img2;
    GtkWidget *img3;
    GtkWidget *info;
    GtkWidget *search_name_entry;
    GtkWidget *search_num_entry;
    GtkWidget *filter_ability_entry;
    GtkWidget *filter_stat_entry;

    // Main Window Buttons
    GtkButton *up1_but;
    GtkButton *up10_but;
    GtkButton *up100_but;
    GtkButton *down1_but;
    GtkButton *down10_but;
    GtkButton *down100_but;
    GtkButton *search_but;
    GtkButton *sort_but;
    GtkButton *filter_but;
    GtkButton *revert_but;

    // Sort Window Buttons
    GtkButton *sort_num_button;
    GtkButton *sort_name_button;
    GtkButton *sort_ptype_button;
    GtkButton *sort_stype_button;
    GtkButton *sort_ability1_button;
    GtkButton *sort_ability2_button;
    GtkButton *sort_hability_button;
    GtkButton *sort_height_button;
    GtkButton *sort_weight_button;
    GtkButton *sort_HP_button;
    GtkButton *sort_ATK_button;
    GtkButton *sort_DEF_button;
    GtkButton *sort_SPATK_button;
    GtkButton *sort_SPDEF_button;
    GtkButton *sort_SPEED_button;
    GtkButton *sort_back_button;

    // Search window buttons
    GtkButton *search_name_button;
    GtkButton *search_num_button;
    GtkButton *search_back_button;

    //Secondary Search Window Buttons
    GtkButton *search_name_back_button;
    GtkButton *search_name_search_button;

    GtkButton *search_num_back_button;
    GtkButton *search_num_search_button;

    // Filter window buttons
    GtkButton *filter_ptype_button;
    GtkButton *filter_stype_button;
    GtkButton *filter_ability_button;
    GtkButton *filter_hability_button;
    GtkButton *filter_height_button;
    GtkButton *filter_weight_button;
    GtkButton *filter_back_button;
    GtkButton *filter_HP_button;
    GtkButton *filter_ATK_button;
    GtkButton *filter_DEF_button;
    GtkButton *filter_SPATK_button;
    GtkButton *filter_SPDEF_button;
    GtkButton *filter_SPEED_button;

    // Secondary Filter Window Buttons


	GError *error = NULL;

    // Initialize GTK
    gtk_init(&argc, &argv);

    // Create new GTK Builder
    builder = gtk_builder_new();

    // Load base UI from glade file
	if(!gtk_builder_add_from_file(builder, "src/glade_files/poke_viz.glade", &error)){

        g_warning("%s", error->message);
        g_free(error);
        return(1);
    }

    // Grab window pointer from the UI
    window = GTK_WIDGET(gtk_builder_get_object(builder, "pokedex"));
    sort_window = GTK_WIDGET(gtk_builder_get_object(builder, "sort_window"));
    search_window = GTK_WIDGET(gtk_builder_get_object(builder, "search_window"));
    filter_window = GTK_WIDGET(gtk_builder_get_object(builder, "filter_window"));
    search_num_window = GTK_WIDGET(gtk_builder_get_object(builder, "search_num_window"));
    search_name_window = GTK_WIDGET(gtk_builder_get_object(builder, "search_name_window"));
    filter_ability_window = GTK_WIDGET(gtk_builder_get_object(builder, "filter_ability_window"));
    filter_stats_window = GTK_WIDGET(gtk_builder_get_object(builder, "filter_stats_window"));
    filter_type_window = GTK_WIDGET(gtk_builder_get_object(builder, "filter_type_window"));

    // Set entries to entries
    search_name_entry = GTK_WIDGET(gtk_builder_get_object(builder, "search_name_entry"));
    search_num_entry = GTK_WIDGET(gtk_builder_get_object(builder, "search_num_entry"));
    filter_ability_entry = GTK_WIDGET(gtk_builder_get_object(builder, "filter_ability_entry"));
    filter_stat_entry = GTK_WIDGET(gtk_builder_get_object(builder, "filter_stat_entry"));

    // Restrict num entries to numbers or digits !! Doesn't seem to work ...
    gtk_entry_set_input_purpose (GTK_ENTRY(search_num_entry), GTK_INPUT_PURPOSE_DIGITS);
    gtk_entry_set_input_purpose (GTK_ENTRY(filter_stat_entry), GTK_INPUT_PURPOSE_NUMBER);

    // Set Images to correct images
    img1 = GTK_WIDGET(gtk_builder_get_object(builder, "IMG1"));
    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    img2 = GTK_WIDGET(gtk_builder_get_object(builder, "IMG2"));
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    img3 = GTK_WIDGET(gtk_builder_get_object(builder, "IMG3"));
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    // Set up text view object
    info = GTK_WIDGET(gtk_builder_get_object(builder, "info"));
    GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
    string poke_info = mod[img_2_index]->getInfo();
    gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());

    // Create data to pass to button functions
    vector<void*> pass_data;

    pass_data.push_back(static_cast<void*>(img1));
    pass_data.push_back(static_cast<void*>(img2));
    pass_data.push_back(static_cast<void*>(img3));
    pass_data.push_back(static_cast<void*>(&dex));
    pass_data.push_back(static_cast<void*>(&img_1_index));
    pass_data.push_back(static_cast<void*>(&img_2_index));
    pass_data.push_back(static_cast<void*>(&img_3_index));
    pass_data.push_back(static_cast<void*>(&MAX_INDEX));
    pass_data.push_back(static_cast<void*>(info));
    pass_data.push_back(static_cast<void*>(sort_window));
    pass_data.push_back(static_cast<void*>(search_window));
    pass_data.push_back(static_cast<void*>(filter_window));
    pass_data.push_back(static_cast<void*>(search_name_window));
    pass_data.push_back(static_cast<void*>(search_num_window));
    pass_data.push_back(static_cast<void*>(search_name_entry));
    pass_data.push_back(static_cast<void*>(search_num_entry));
    pass_data.push_back(static_cast<void*>(filter_ability_window));
    pass_data.push_back(static_cast<void*>(filter_stats_window));
    pass_data.push_back(static_cast<void*>(filter_type_window));
    pass_data.push_back(static_cast<void*>(filter_ability_entry));
    pass_data.push_back(static_cast<void*>(filter_stat_entry));

    // Connect main window buttons to functions
    up1_but = GTK_BUTTON(gtk_builder_get_object(builder, "up"));
    g_signal_connect(up1_but, "clicked", G_CALLBACK(up1_pressed), (gpointer) &pass_data);
    up10_but = GTK_BUTTON(gtk_builder_get_object(builder, "up10"));
    g_signal_connect(up10_but, "clicked", G_CALLBACK(up10_pressed), (gpointer) &pass_data);
    up100_but = GTK_BUTTON(gtk_builder_get_object(builder, "up100"));
    g_signal_connect(up100_but, "clicked", G_CALLBACK(up100_pressed), (gpointer) &pass_data);
    down1_but = GTK_BUTTON(gtk_builder_get_object(builder, "down"));
    g_signal_connect(down1_but, "clicked", G_CALLBACK(down1_pressed), (gpointer) &pass_data);
    down10_but = GTK_BUTTON(gtk_builder_get_object(builder, "down10"));
    g_signal_connect(down10_but, "clicked", G_CALLBACK(down10_pressed), (gpointer) &pass_data);
    down100_but = GTK_BUTTON(gtk_builder_get_object(builder, "down100"));
    g_signal_connect(down100_but, "clicked", G_CALLBACK(down100_pressed), (gpointer) &pass_data);
    search_but = GTK_BUTTON(gtk_builder_get_object(builder, "search"));
    g_signal_connect(search_but, "clicked", G_CALLBACK(search_pressed), (gpointer) &pass_data);
    sort_but = GTK_BUTTON(gtk_builder_get_object(builder, "sort"));
    g_signal_connect(sort_but, "clicked", G_CALLBACK(sort_pressed), (gpointer) &pass_data);
    filter_but = GTK_BUTTON(gtk_builder_get_object(builder, "filter"));
    g_signal_connect(filter_but, "clicked", G_CALLBACK(filter_pressed), (gpointer) &pass_data);
    revert_but = GTK_BUTTON(gtk_builder_get_object(builder, "revert"));
    g_signal_connect(revert_but, "clicked", G_CALLBACK(revert_pressed), (gpointer) &pass_data);

    // Connect sort window buttons to functions
    sort_num_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_num_button"));
    g_signal_connect(sort_num_button, "clicked", G_CALLBACK(sort_num_pressed), (gpointer) &pass_data);
    sort_name_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_name_button"));
    g_signal_connect(sort_name_button, "clicked", G_CALLBACK(sort_name_pressed), (gpointer) &pass_data);
    sort_ptype_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_ptype_button"));
    g_signal_connect(sort_ptype_button, "clicked", G_CALLBACK(sort_ptype_pressed), (gpointer) &pass_data);
    sort_stype_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_stype_button"));
    g_signal_connect(sort_stype_button, "clicked", G_CALLBACK(sort_stype_pressed), (gpointer) &pass_data);
    sort_ability1_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_ability1_button"));
    g_signal_connect(sort_ability1_button, "clicked", G_CALLBACK(sort_ability1_pressed), (gpointer) &pass_data);
    sort_ability2_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_ability2_button"));
    g_signal_connect(sort_ability2_button, "clicked", G_CALLBACK(sort_ability2_pressed), (gpointer) &pass_data);
    sort_hability_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_hability_button"));
    g_signal_connect(sort_hability_button, "clicked", G_CALLBACK(sort_hability_pressed), (gpointer) &pass_data);
    sort_height_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_height_button"));
    g_signal_connect(sort_height_button, "clicked", G_CALLBACK(sort_height_pressed), (gpointer) &pass_data);
    sort_weight_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_weight_button"));
    g_signal_connect(sort_weight_button, "clicked", G_CALLBACK(sort_weight_pressed), (gpointer) &pass_data);
    sort_HP_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_HP_button"));
    g_signal_connect(sort_HP_button, "clicked", G_CALLBACK(sort_HP_pressed), (gpointer) &pass_data);
    sort_ATK_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_ATK_button"));
    g_signal_connect(sort_ATK_button, "clicked", G_CALLBACK(sort_ATK_pressed), (gpointer) &pass_data);
    sort_DEF_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_DEF_button"));
    g_signal_connect(sort_DEF_button, "clicked", G_CALLBACK(sort_DEF_pressed), (gpointer) &pass_data);
    sort_SPATK_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_SPATK_button"));
    g_signal_connect(sort_SPATK_button, "clicked", G_CALLBACK(sort_SPATK_pressed), (gpointer) &pass_data);
    sort_SPDEF_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_SPDEF_button"));
    g_signal_connect(sort_SPDEF_button, "clicked", G_CALLBACK(sort_SPDEF_pressed), (gpointer) &pass_data);
    sort_SPEED_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_SPEED_button"));
    g_signal_connect(sort_SPEED_button, "clicked", G_CALLBACK(sort_SPEED_pressed), (gpointer) &pass_data);
    sort_back_button = GTK_BUTTON(gtk_builder_get_object(builder, "sort_back_button"));
    g_signal_connect(sort_back_button, "clicked", G_CALLBACK(sort_back_pressed), (gpointer) &pass_data);

    // Connect search window buttons to functions
    search_name_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_name_button"));
    g_signal_connect(search_name_button, "clicked", G_CALLBACK(search_name_pressed), (gpointer) &pass_data);
    search_num_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_num_button"));
    g_signal_connect(search_num_button, "clicked", G_CALLBACK(search_num_pressed), (gpointer) &pass_data);
    search_back_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_back_button"));
    g_signal_connect(search_back_button, "clicked", G_CALLBACK(search_back_pressed), (gpointer) &pass_data);
    search_name_back_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_name_back_button"));
    g_signal_connect(search_name_back_button, "clicked", G_CALLBACK(search_name_back_pressed), (gpointer) &pass_data);
    search_num_back_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_num_back_button"));
    g_signal_connect(search_num_back_button, "clicked", G_CALLBACK(search_num_back_pressed), (gpointer) &pass_data);
    search_name_search_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_name_search_button"));
    g_signal_connect(search_name_search_button, "clicked", G_CALLBACK(search_name_search_pressed), (gpointer) &pass_data);
    search_num_search_button = GTK_BUTTON(gtk_builder_get_object(builder, "search_num_search_button"));
    g_signal_connect(search_num_search_button, "clicked", G_CALLBACK(search_num_search_pressed), (gpointer) &pass_data);

    // Connect filter window buttons to functions
    filter_ptype_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_ptype_button"));
    g_signal_connect(filter_ptype_button, "clicked", G_CALLBACK(filter_ptype_pressed), (gpointer) &pass_data);
    filter_stype_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_stype_button"));
    g_signal_connect(filter_stype_button, "clicked", G_CALLBACK(filter_stype_pressed), (gpointer) &pass_data);
    filter_ability_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_ability_button"));
    g_signal_connect(filter_ability_button, "clicked", G_CALLBACK(filter_ability_pressed), (gpointer) &pass_data);
    filter_hability_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_hability_button"));
    g_signal_connect(filter_hability_button, "clicked", G_CALLBACK(filter_hability_pressed), (gpointer) &pass_data);
    filter_height_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_height_button"));
    g_signal_connect(filter_height_button, "clicked", G_CALLBACK(filter_height_pressed), (gpointer) &pass_data);
    filter_weight_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_weight_button"));
    g_signal_connect(filter_weight_button, "clicked", G_CALLBACK(filter_weight_pressed), (gpointer) &pass_data);
    filter_back_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_back_button"));
    g_signal_connect(filter_back_button, "clicked", G_CALLBACK(filter_back_pressed), (gpointer) &pass_data);
    filter_HP_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_HP_button"));
    g_signal_connect(filter_HP_button, "clicked", G_CALLBACK(filter_HP_pressed), (gpointer) &pass_data);
    filter_ATK_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_ATK_button"));
    g_signal_connect(filter_ATK_button, "clicked", G_CALLBACK(filter_ATK_pressed), (gpointer) &pass_data);
    filter_DEF_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_DEF_button"));
    g_signal_connect(filter_DEF_button, "clicked", G_CALLBACK(filter_DEF_pressed), (gpointer) &pass_data);
    filter_SPATK_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_SPATK_button"));
    g_signal_connect(filter_SPATK_button, "clicked", G_CALLBACK(filter_SPATK_pressed), (gpointer) &pass_data);
    filter_SPDEF_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_SPDEF_button"));
    g_signal_connect(filter_SPDEF_button, "clicked", G_CALLBACK(filter_SPDEF_pressed), (gpointer) &pass_data);
    filter_SPEED_button = GTK_BUTTON(gtk_builder_get_object(builder, "filter_SPEED_button"));
    g_signal_connect(filter_SPEED_button, "clicked", G_CALLBACK(filter_SPEED_pressed), (gpointer) &pass_data);

    // Connect builder signals
    gtk_builder_connect_signals(builder, NULL);

    // Show the window with all its widgets
    gtk_widget_show(window);

    // Start main window operation
    gtk_main();

    return 0;
}

// Utility Functions ----------------------------------------------------------

// Check for usage requirements
void usageCheck(const int &argc, char *argv[])
{
    bool usage_error = false;

    if(argc > 1){
        usage_error = true;
    }

    if(usage_error){

        printUsage();
        exit(1);
    }
}

// Print usage message
void printUsage()
{
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Usage: ./bin/pokedex                          \n");
}
