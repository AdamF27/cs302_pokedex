//-----------------------------------------------------------------------------
// File: pokedex.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/23/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Function definitions for pokedex class.
//-----------------------------------------------------------------------------

#include <iostream>
#include <cstdio>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "pokedex.hpp"
#include "poke_sort.hpp"
#include "poke_filter.hpp"
#include "poke_search.hpp"
#include "utility.hpp"

using namespace std;

// Constructors / Destructors -------------------------------------------------

Pokedex::Pokedex()
{
    // Open the database file
    fstream db_file;
    db_file.open("./database/pokemon.txt");

    // Parse the database and create pokemon objects if file was opened
    // successfully.
    if(db_file.is_open()){

        string line;
        istringstream ss;

        while(getline(db_file, line)){

            // Ignore comment lines
            if(line[0] == '#') continue;

            Pokemon* p = new Pokemon();
            Pokemon* q = p;

            // Temp variables for loading values
            int i_val;
            double d_val;

            // Set Number
            ss.str(line);
            ss >> i_val;
            p->setNum(i_val);
            ss.clear();

            // Set Name
            getline(db_file, line);

            p->setName(leftTrim(line));

            // Set Type 1
            getline(db_file, line);

            p->setType(0, leftTrim(line));

            // Set Type 2
            getline(db_file, line);

            p->setType(1, leftTrim(line));

            // Set Ability 1
            getline(db_file, line);

            p->setAbility(0, leftTrim(line));

            // Set Ability 2
            getline(db_file, line);

            p->setAbility(1, leftTrim(line));

            // Set Hidden Ability
            getline(db_file, line);

            p->setAbility(2, leftTrim(line));

            // Set Height
            getline(db_file, line);

            ss.str(line);
            ss >> d_val;
            p->setHeight(d_val);
            ss.clear();

            // Set Weight
            getline(db_file, line);

            ss.str(line);
            ss >> d_val;
            p->setWeight(d_val);
            ss.clear();

            // Set All Stats
            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(0, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(1, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(2, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(3, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(4, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(5, i_val);
            ss.clear();

            // Put new pokemon in master_dex and mod_dex vectors
            // These are initially the same.
            master_dex.push_back(p);
            mod_dex.push_back(q);
        }
    }
    else{

        fprintf(stdout, "Default Pokedex constructor used. This requires a pokemon.txt \n");
        fprintf(stdout, "file stored in database/. Ensure this is the case and run the \n");
        fprintf(stdout, "pokedex from the root directory, or use the other constructor.\n");
        exit(1);
    }
}

Pokedex::Pokedex(const string& db_path)
{

    // Open the database file
    fstream db_file;
    db_file.open(db_path);

    // Parse the database and create pokemon objects if file was opened
    // successfully.
    if(db_file.is_open()){

        string line;
        istringstream ss;

        while(getline(db_file, line)){

            // Ignore comment lines
            if(line[0] == '#') continue;

            Pokemon* p = new Pokemon();
            Pokemon* q = p;

            // Temp variables for loading values
            int i_val;
            double d_val;

            // Set Number
            ss.str(line);
            ss >> i_val;
            p->setNum(i_val);
            ss.clear();

            // Set Name
            getline(db_file, line);

            p->setName(leftTrim(line));

            // Set Type 1
            getline(db_file, line);

            p->setType(0, leftTrim(line));

            // Set Type 2
            getline(db_file, line);

            p->setType(1, leftTrim(line));

            // Set Ability 1
            getline(db_file, line);

            p->setAbility(0, leftTrim(line));

            // Set Ability 2
            getline(db_file, line);

            p->setAbility(1, leftTrim(line));

            // Set Hidden Ability
            getline(db_file, line);

            p->setAbility(2, leftTrim(line));

            // Set Height
            getline(db_file, line);

            ss.str(line);
            ss >> d_val;
            p->setHeight(d_val);
            ss.clear();

            // Set Weight
            getline(db_file, line);

            ss.str(line);
            ss >> d_val;
            p->setWeight(d_val);
            ss.clear();

            // Set All Stats
            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(0, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(1, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(2, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(3, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(4, i_val);
            ss.clear();

            getline(db_file, line);

            ss.str(line);
            ss >> i_val;
            p->setStat(5, i_val);
            ss.clear();

            // Put new pokemon in master_dex and mod_dex vectors
            // These are initially the same.
            master_dex.push_back(p);
            mod_dex.push_back(q);
        }
    }
    else{

        fprintf(stdout, "You have specified a database file that cannot be \n");
        fprintf(stdout, "cannot be opened. Please ensure it exists and the \n");
        fprintf(stdout, "path is correct and try again.                    \n");
        exit(1);
    }
}

Pokedex::~Pokedex()
{
    for(unsigned int i = 0; i < master_dex.size(); i++){

        delete master_dex[i];
    }
}

// Operator Overloading -------------------------------------------------------

Pokedex& Pokedex::operator=(const Pokedex &dex)
{
    if(this == &dex){

        return *this;
    }

    vector<Pokemon*> rhs_mas_dex = dex.getMasterDex();
    vector<Pokemon*> rhs_mod_dex = dex.getModDex();

    // Delete old entries
    for(size_t i = 0; i < master_dex.size(); i++){

        if(master_dex[i] != NULL){

            delete master_dex[i];
        }
    }

    // Resize vectors
    master_dex.resize(rhs_mas_dex.size());
    mod_dex.resize(rhs_mas_dex.size());

    // Copy their data, mod dex is copy of master initially.
    for(size_t i = 0; i < rhs_mas_dex.size(); i++){

        Pokemon* p = new Pokemon(*rhs_mas_dex[i]);
        master_dex[i] = p;
        mod_dex[i] = p;
    }

    return *this;
}

// Accessors ------------------------------------------------------------------

vector<Pokemon*> Pokedex::getMasterDex() const
{
    return master_dex;
}

vector<Pokemon*> Pokedex::getModDex() const
{
    return mod_dex;
}

// Modifiers ------------------------------------------------------------------

// Utility Functions ----------------------------------------------------------

void Pokedex::printDex()
{
    for(unsigned int i = 0; i < mod_dex.size(); i++){

        mod_dex[i]->printData();
    }
}

void Pokedex::sortDex(const int& s_param)
{
    switch(s_param){

        case  1: sortNum(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  2: sortName(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  3: sortType1(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  4: sortType2(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  5: sortAbility1(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  6: sortAbility2(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  7: sortHiddenAbility(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  8: sortHeight(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case  9: sortWeight(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case 10: sortHP(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case 11: sortAtk(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case 12: sortDef(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case 13: sortSpAtk(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case 14: sortSpDef(mod_dex, 0, int(mod_dex.size()-1));
                 break;
        case 15: sortSpeed(mod_dex, 0, int(mod_dex.size()-1));
                 break;

        default: break;
    }
}

void Pokedex::filterDex(const int& f_param)
{
    switch(f_param){

        case  1: filterType(0, mod_dex);
                 break;
        case  2: filterType(1, mod_dex);
                 break;
        case  3: filterAbility(mod_dex);
                 break;
        case  4: filterHAbility(mod_dex);
                 break;
        case  5: filterHeight(mod_dex);
                 break;
        case  6: filterWeight(mod_dex);
                 break;
        case  7: filterStat(0, mod_dex);
                 break;
        case  8: filterStat(1, mod_dex);
                 break;
        case  9: filterStat(2, mod_dex);
                 break;
        case 10: filterStat(3, mod_dex);
                 break;
        case 11: filterStat(4, mod_dex);
                 break;
        case 12: filterStat(5, mod_dex);
                 break;

        default: break;
    }
}

void Pokedex::searchDex(const int& s_param)
{
    switch(s_param){

        case 1: searchNum(mod_dex);
                break;
        case 2: searchName(mod_dex);
                break;

        default: break;
    }
}

// Only used in GUI since prompt isnt necessary
void Pokedex::searchDexName(const string& name)
{
    searchName(mod_dex, name);
}

// Only used in GUI since prompt isnt necessary
void Pokedex::searchDexNum(const int& num)
{
    searchNum(mod_dex, num);
}


void Pokedex::revertDex()
{
    mod_dex.resize(master_dex.size());

    for(size_t i = 0; i < master_dex.size(); i++){

        mod_dex[i] = master_dex[i];
    }
}
