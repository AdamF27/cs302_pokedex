//-----------------------------------------------------------------------------
// File: button.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 4/22/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Button function definitions
//-----------------------------------------------------------------------------

#include "buttons.hpp"
#include "pokedex.hpp"
#include "pokemon.hpp"
#include "utility.hpp"

using namespace std;

// GUI Button Functions -------------------------------------------------------


///////////////////////////////////////////////////////////////////////////////
// MAIN WINDOW BUTTONS ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void up1_pressed(GtkWidget *widget, gpointer data){

    // String paths
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    // Grab all the arguments passed with the gpointer
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    // Grab mod dex to update images and info
    vector<Pokemon*> mod = dex->getModDex();

    *max = mod.size() - 1;

    if(!(*img_1_in - 1 < -1)){

        *img_1_in -= 1;
        *img_2_in -= 1;
        *img_3_in -= 1;
    }
    else{

        *img_1_in = -1;
        *img_2_in = 0;
        *img_3_in = 1;
    }

    // Set path to sprites

    if(*img_1_in == -1){

        img_1_path = img_1_path + "invalid.png";
    }
    else{

        img_1_path.append(to_string(mod[*img_1_in]->getNum()));
        img_1_path.append(".png");
    }

    if(*img_2_in > (*max)){

        img_2_path = img_2_path + "invalid.png";

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{

        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in > (*max)){

        img_3_path = img_3_path + "invalid.png";
    }
    else{

        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

void up10_pressed(GtkWidget *widget, gpointer data){

    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    vector<Pokemon*> mod = dex->getModDex();

    *max = mod.size() - 1;

    if(!(*img_1_in - 10 < -1)){

        *img_1_in -= 10;
        *img_2_in -= 10;
        *img_3_in -= 10;
    }
    else{

        *img_1_in = -1;
        *img_2_in = 0;
        *img_3_in = 1;
    }

    // Set path to sprites

    if(*img_1_in == -1){

        img_1_path = img_1_path + "invalid.png";
    }
    else{

        img_1_path.append(to_string(mod[*img_1_in]->getNum()));
        img_1_path.append(".png");
    }

    if(*img_2_in > (*max)){

        img_2_path = img_2_path + "invalid.png";

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{

        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in > (*max)){

        img_3_path = img_3_path + "invalid.png";
    }
    else{

        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

void up100_pressed(GtkWidget *widget, gpointer data){

    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    vector<Pokemon*> mod = dex->getModDex();

    *max = mod.size() - 1;

    if(!(*img_1_in - 100 < -1)){

        *img_1_in -= 100;
        *img_2_in -= 100;
        *img_3_in -= 100;
    }
    else{

        *img_1_in = -1;
        *img_2_in = 0;
        *img_3_in = 1;
    }

    // Set path to sprites

    if(*img_1_in == -1){

        img_1_path = img_1_path + "invalid.png";
    }
    else{

        img_1_path.append(to_string(mod[*img_1_in]->getNum()));
        img_1_path.append(".png");
    }

    if(*img_2_in > (*max)){

        img_2_path = img_2_path + "invalid.png";

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{

        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in > (*max)){

        img_3_path = img_3_path + "invalid.png";
    }
    else{

        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

void down1_pressed(GtkWidget *widget, gpointer data){

    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    vector<Pokemon*> mod = dex->getModDex();

    *max = mod.size() - 1;

    if(!(*img_3_in + 1 > (*max) + 1)){

        *img_1_in += 1;
        *img_2_in += 1;
        *img_3_in += 1;
    }
    else{

        *img_1_in = *max - 1;
        *img_2_in = *max;
        *img_3_in = *max + 1;
    }

    // Set path to sprites

    if(*img_3_in == *max + 1){

        img_3_path = img_3_path + "invalid.png";
    }
    else{

        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }

    if(*img_1_in < 0){

        img_1_path = img_1_path + "invalid.png";
    }
    else{

        img_1_path.append(to_string(mod[*img_1_in]->getNum()));
        img_1_path.append(".png");
    }

    if(*img_2_in < 0){

        img_2_path = img_2_path + "invalid.png";

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{

        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    // Update sprites
    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

void down10_pressed(GtkWidget *widget, gpointer data){

    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    vector<Pokemon*> mod = dex->getModDex();

    *max = mod.size() - 1;

    if(!(*img_3_in + 10 > (*max) + 1)){

        *img_1_in += 10;
        *img_2_in += 10;
        *img_3_in += 10;
    }
    else{

        *img_1_in = *max - 1;
        *img_2_in = *max;
        *img_3_in = *max + 1;
    }

    // Set path to sprites

    if(*img_3_in == *max + 1){

        img_3_path = img_3_path + "invalid.png";
    }
    else{

        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }

    if(*img_1_in < 0){

        img_1_path = img_1_path + "invalid.png";
    }
    else{

        img_1_path.append(to_string(mod[*img_1_in]->getNum()));
        img_1_path.append(".png");
    }

    if(*img_2_in < 0){

        img_2_path = img_2_path + "invalid.png";

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{

        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

void down100_pressed(GtkWidget *widget, gpointer data){

    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    vector<Pokemon*> mod = dex->getModDex();

    *max = mod.size() - 1;

    if(!(*img_3_in + 100 > (*max) + 1)){

        *img_1_in += 100;
        *img_2_in += 100;
        *img_3_in += 100;
    }
    else{

        *img_1_in = *max - 1;
        *img_2_in = *max;
        *img_3_in = *max + 1;
    }

    // Set path to sprites

    if(*img_3_in == *max + 1){

        img_3_path = img_3_path + "invalid.png";
    }
    else{

        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }

    if(*img_1_in < 0){

        img_1_path = img_1_path + "invalid.png";
    }
    else{

        img_1_path.append(to_string(mod[*img_1_in]->getNum()));
        img_1_path.append(".png");
    }

    if(*img_2_in < 0){

        img_2_path = img_2_path + "invalid.png";

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{

        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Update displayed info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

void search_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *search_window = (GtkWidget *)(*vdata)[10];

    gtk_widget_show( search_window );
}

void sort_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    gtk_widget_show( sort_window );
}

void filter_pressed(GtkWidget *widget, gpointer data)
{
    // Uncomment when all filter functions are done

    //vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    //GtkWidget *filter_window = (GtkWidget *)(*vdata)[11];

    //gtk_widget_show( filter_window );
}

void revert_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];

    dex->revertDex();

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());
}

///////////////////////////////////////////////////////////////////////////////
// SORT WINDOW BUTTONS ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void sort_num_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(1);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_name_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(2);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_ptype_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(3);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_stype_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(4);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_ability1_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(5);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_ability2_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(6);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_hability_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(7);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_height_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(8);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_weight_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(9);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_HP_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(10);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_ATK_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(11);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);
}

void sort_DEF_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(12);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);

}

void sort_SPATK_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(13);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);

}

void sort_SPDEF_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(14);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);

}

void sort_SPEED_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    dex->sortDex(15);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(sort_window);

}

void sort_back_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *sort_window = (GtkWidget *)(*vdata)[9];

    gtk_widget_hide((sort_window));
}

///////////////////////////////////////////////////////////////////////////////
// SEARCH WINDOW BUTTONS //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void search_name_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *search_window = (GtkWidget *)(*vdata)[10];
    GtkWidget *search_name_window = (GtkWidget *)(*vdata)[12];

    gtk_widget_hide( search_window );
    gtk_widget_show( search_name_window );
}

void search_num_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *search_window = (GtkWidget *)(*vdata)[10];
    GtkWidget *search_num_window = (GtkWidget *)(*vdata)[13];

    gtk_widget_hide( search_window );
    gtk_widget_show( search_num_window );
}

void search_back_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *search_window = (GtkWidget *)(*vdata)[10];

    gtk_widget_hide( search_window );
}

void search_name_search_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *search_name_window = (GtkWidget *)(*vdata)[12];
    GtkWidget *search_name_entry = (GtkWidget *)(*vdata)[14];

    string name = gtk_entry_get_text(GTK_ENTRY(search_name_entry));

    gtk_entry_set_text(GTK_ENTRY(search_name_entry), "");

    dex->searchDexName(name);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(search_name_window);
}

void search_name_back_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *search_window = (GtkWidget *)(*vdata)[10];
    GtkWidget *search_name_window = (GtkWidget *)(*vdata)[12];

    gtk_widget_hide( search_name_window );
    gtk_widget_show( search_window );
}

void search_num_search_pressed(GtkWidget *widget, gpointer data)
{
    string img_1_path = "sprites/";
    string img_2_path = "sprites/";
    string img_3_path = "sprites/";

    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *img1 = (GtkWidget *)(*vdata)[0];
    GtkWidget *img2 = (GtkWidget *)(*vdata)[1];
    GtkWidget *img3 = (GtkWidget *)(*vdata)[2];
    Pokedex * dex = (Pokedex *)(*vdata)[3];
    int *img_1_in = (int *)(*vdata)[4];
    int *img_2_in = (int *)(*vdata)[5];
    int *img_3_in = (int *)(*vdata)[6];
    int *max = (int *)(*vdata)[7];
    GtkWidget *info = (GtkWidget *)(*vdata)[8];
    GtkWidget *search_num_window = (GtkWidget *)(*vdata)[13];
    GtkWidget *search_num_entry = (GtkWidget *)(*vdata)[15];

    int num = -1;

    string s_num = (gtk_entry_get_text(GTK_ENTRY(search_num_entry)));

    gtk_entry_set_text(GTK_ENTRY(search_num_entry), "");

    // Prevents running stoi on non numeric input
    if(!(isLetters(s_num))){

        num = stoi(s_num);
    }

    dex->searchDexNum(num);

    vector<Pokemon*> mod = dex->getModDex();

    *img_1_in = -1;
    *img_2_in = 0;
    *img_3_in = 1;

    *max = mod.size() - 1;

    img_1_path = img_1_path + "invalid.png";

    if(*img_2_in <= *max){
        img_2_path.append(to_string(mod[*img_2_in]->getNum()));
        img_2_path.append(".png");

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = mod[*img_2_in]->getInfo();
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }
    else{
        img_2_path = img_2_path + "invalid.png";

        // Set display info
        GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(info));
        string poke_info = "";
        gtk_text_buffer_set_text(buf, poke_info.c_str(), poke_info.size());
    }

    if(*img_3_in <= *max){
        img_3_path.append(to_string(mod[*img_3_in]->getNum()));
        img_3_path.append(".png");
    }
    else{
        img_3_path = img_3_path + "invalid.png";
    }

    // Update sprites

    gtk_image_set_from_file(GTK_IMAGE(img1), img_1_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img2), img_2_path.c_str());
    gtk_image_set_from_file(GTK_IMAGE(img3), img_3_path.c_str());

    gtk_widget_hide(search_num_window);

}

void search_num_back_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *search_window = (GtkWidget *)(*vdata)[10];
    GtkWidget *search_num_window = (GtkWidget *)(*vdata)[13];

    gtk_widget_hide( search_num_window );
    gtk_widget_show( search_window );
}

///////////////////////////////////////////////////////////////////////////////
// FILTER WINDOW BUTTONS //////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void filter_ptype_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_stype_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_ability_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_hability_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_height_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_weight_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_back_pressed(GtkWidget *widget, gpointer data)
{
    vector<void*> *vdata = reinterpret_cast<vector<void*>*>(data);

    GtkWidget *filter_window = (GtkWidget *)(*vdata)[11];

    gtk_widget_hide((filter_window));
}

void filter_HP_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_ATK_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_DEF_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_SPATK_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_SPDEF_pressed(GtkWidget *widget, gpointer data)
{

}

void filter_SPEED_pressed(GtkWidget *widget, gpointer data)
{

}
