//-----------------------------------------------------------------------------
// File: poke_filter.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/30/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Function definitions for poke filtering
//-----------------------------------------------------------------------------

#include <cstdio>
#include <iostream>
#include <cctype>

#include "poke_filter.hpp"
#include "utility.hpp"

// Filter Functions -----------------------------------------------------------

void filterType(const int &t, vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    // Primary type
    if(t == 0){

        string type = typePrompt();

        if(type == "INVALID"){

            badInputPrompt();
            return;
        }

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getType(0) == type){

                newv.push_back(mod[i]);
            }
        }
        mod = newv;
    }
    // Secondary Type
    else if(t == 1){

        string type = typePrompt();

        if(type == "INVALID"){

            badInputPrompt();
            return;
        }

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getType(1) == type){

                newv.push_back(mod[i]);
            }
        }
        mod = newv;
    }
    // Invalid
    else{

        fprintf(stdout, "Error in function call filterType(): Expected t\n");
        fprintf(stdout, "value of 0 or 1. Exiting.                      \n");
        exit(1);
    }
}

void filterAbility(vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    string ability = abilityPrompt();

    if(ability == "None"){

        mod.resize(0);
    }
    else if(!(isLetters(ability))){

        mod.resize(0);
    }
    else{

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getAbility(0) == ability || mod[i]->getAbility(1) == ability){

                newv.push_back(mod[i]);
            }
        }
    }

    mod = newv;
}

void filterHAbility(vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    string ability = abilityPrompt();

    if(ability == "None"){

        mod.resize(0);
    }
    else if(!(isLetters(ability))){

        mod.resize(0);
    }
    else{

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getAbility(2) != ability){

                newv.push_back(mod[i]);
            }
        }
    }

    mod = newv;
}

void filterHeight(vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    int comp;
    double val;

    heightPrompt(comp, val);

    if(comp == -1){

        badInputPrompt();
        return;
    }
    else if (comp == 1){

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getHeight() < val){

                newv.push_back(mod[i]);
            }
        }
    }
    else if (comp == 2){

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getHeight() > val){

                newv.push_back(mod[i]);
            }
        }
    }
    else{

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getHeight() == val){

                newv.push_back(mod[i]);
            }
        }
    }

    mod = newv;
}

void filterWeight(vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    int comp;
    double val;

    weightPrompt(comp, val);

    if(comp == -1){

        badInputPrompt();
        return;
    }
    else if (comp == 1){

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getWeight() < val){

                newv.push_back(mod[i]);
            }
        }
    }
    else if (comp == 2){

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getWeight() > val){

                newv.push_back(mod[i]);
            }
        }
    }
    else{

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getWeight() == val){

                newv.push_back(mod[i]);
            }
        }
    }

    mod = newv;
}

void filterStat(const int &s, vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    int comp;
    int val;

    statPrompt(comp, val);

    if(comp == -1){

        badInputPrompt();
        return;
    }
    else if (comp == 1){

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getStat(s) < val){

                newv.push_back(mod[i]);
            }
        }
    }
    else if (comp == 2){

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getStat(s) > val){

                newv.push_back(mod[i]);
            }
        }
    }
    else{

        for(unsigned int i = 0; i < mod.size(); i++){

            if(mod[i]->getStat(s) == val){

                newv.push_back(mod[i]);
            }
        }
    }

    mod = newv;
}

// Necessary Prompts ----------------------------------------------------------

string typePrompt()
{
    int choice = 0;
    string type;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to the  \n");
    fprintf(stdout, "Type to filter by:                            \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1.  Normal                                    \n");
    fprintf(stdout, "2.  Fight                                     \n");
    fprintf(stdout, "3.  Flying                                    \n");
    fprintf(stdout, "4.  Poison                                    \n");
    fprintf(stdout, "5.  Ground                                    \n");
    fprintf(stdout, "6.  Rock                                      \n");
    fprintf(stdout, "7.  Bug                                       \n");
    fprintf(stdout, "8.  Ghost                                     \n");
    fprintf(stdout, "9.  Steel                                     \n");
    fprintf(stdout, "10. Fire                                      \n");
    fprintf(stdout, "11. Water                                     \n");
    fprintf(stdout, "12. Grass                                     \n");
    fprintf(stdout, "13. Electric                                  \n");
    fprintf(stdout, "14. Psychic                                   \n");
    fprintf(stdout, "15. Ice                                       \n");
    fprintf(stdout, "16. Dragon                                    \n");
    fprintf(stdout, "17. Dark                                      \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    switch(choice){

        case  1: type = "Normal";
                 break;
        case  2: type = "Fight";
                 break;
        case  3: type = "Flying";
                 break;
        case  4: type = "Poison";
                 break;
        case  5: type = "Ground";
                 break;
        case  6: type = "Rock";
                 break;
        case  7: type = "Bug";
                 break;
        case  8: type = "Ghost";
                 break;
        case  9: type = "Steel";
                 break;
        case 10: type = "Fire";
                 break;
        case 11: type = "Water";
                 break;
        case 12: type = "Grass";
                 break;
        case 13: type = "Electric";
                 break;
        case 14: type = "Psychic";
                 break;
        case 15: type = "Ice";
                 break;
        case 16: type = "Dragon";
                 break;
        case 17: type = "Dark";
                 break;
        default: type = "INVALID";
                 break;
    }

    return type;
}

string abilityPrompt()
{
    string ability;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please type the ability you would like to     \n");
    fprintf(stdout, "filter by and press enter:                    \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Ability: ");

    cin >> ability;

    return ability;
}

void heightPrompt(int &comp, double &val)
{
    int choice;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to how  \n");
    fprintf(stdout, "you would like height values filtered.        \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1. Height less than my value.                 \n");
    fprintf(stdout, "2. Height greater than my value.              \n");
    fprintf(stdout, "3. Height equal to my value                   \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    if(choice < 1 || choice > 3){

        comp = -1;
        return;
    }
    else{

        comp = choice;
    }

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter height value you would like to   \n");
    fprintf(stdout, "be compared against.                          \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Value: ");

    cin >> val;

    return;
}

void weightPrompt(int &comp, double &val)
{
    int choice;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to how  \n");
    fprintf(stdout, "you would like weight values filtered.        \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1. Weight less than my value.                 \n");
    fprintf(stdout, "2. Weight greater than my value.              \n");
    fprintf(stdout, "3. Weight equal to my value                   \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    if(choice < 1 || choice > 3){

        comp = -1;
        return;
    }
    else{

        comp = choice;
    }

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter weight value you would like to   \n");
    fprintf(stdout, "be compared against.                          \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Value: ");

    cin >> val;

    return;
}

void statPrompt(int &comp, int &val)
{
    int choice;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to how  \n");
    fprintf(stdout, "you would like stat values filtered.          \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1. Stat less than my value.                   \n");
    fprintf(stdout, "2. Stat greater than my value.                \n");
    fprintf(stdout, "3. Stat equal to my value                     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    if(choice < 1 || choice > 3){

        comp = -1;
        return;
    }
    else{

        comp = choice;
    }

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter stat value you would like to     \n");
    fprintf(stdout, "be compared against.                          \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Value: ");

    cin >> val;

    return;
}
