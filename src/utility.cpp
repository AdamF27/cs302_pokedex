//-----------------------------------------------------------------------------
// File: utility.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/18/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Utility function definitions
//-----------------------------------------------------------------------------

#include <algorithm>
#include <iostream>
#include <cstdio>
#include <locale>

#include "utility.hpp"

using namespace std;

// Remove leading whitespace from strings
// source: https://www.techiedelight.com/trim-string-cpp-remove-leading-trailing-spaces/
string leftTrim(const string &s)
{
    size_t start = s.find_first_not_of(" \n\r\t\f\v");
    return (start == string::npos) ? "" : s.substr(start);
}

void swap_elem(vector<Pokemon*> &dex, const int &i, const int &j)
{
    Pokemon* temp = dex[i];
    dex[i] = dex[j];
    dex[j] = temp;
}

// Waits til enter is pressed then continue execution.
// www.cplusplus.com/forum/articles/7312
void PressEnterToContinue()
{
    cin.sync();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

void badInputPrompt()
{
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "An option was entered that is invalid.        \n");
    fprintf(stdout, "Please press enter then try again.            \n");
    fprintf(stdout, "                                              \n");

    PressEnterToContinue();
}

bool isLetters(string s){

    locale loc;

    for(string::iterator it = s.begin(); it != s.end(); ++it){

        if(!(isalpha(*it, loc))){

            return false;
        }
    }

    return true;
}
