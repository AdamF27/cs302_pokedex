//-----------------------------------------------------------------------------
// File: main.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/18/2019
// Project: CS302 Final Project - Pokedex
//
// Description: This is the main file for the pokedex program.
//-----------------------------------------------------------------------------

#include <cstdio>
#include <iostream>
#include <limits>

#include "pokemon.hpp"
#include "pokedex.hpp"
#include "utility.hpp"

using namespace std;

// Utility Functions ----------------------------------------------------------

void launchDex(Pokedex& dex);
void mainPrompt();
int  sortPrompt();
int  filterPrompt();
int  searchPrompt();
void usageCheck(const int &argc, char *argv[]);
void printUsage();

// Main Execution -------------------------------------------------------------

int main(int argc, char *argv[])
{
    // Check for correct number and format of arguments.
    usageCheck(argc, argv);

    // Create Pokedex
    Pokedex dex;

    // Main text based emulator
    launchDex(dex);

    return 0;
}

// TODO Create all the functionality
void launchDex(Pokedex& dex)
{
    bool done = false;

    while(!done){

        int function = 0;

        mainPrompt();

        cin >> function;

        switch(function){

            int choice;
            // Print database
            case 1: dex.printDex();
                    break;
            // Sort database
            case 2: choice = sortPrompt();
                    if(choice > 15 || choice < 1){
                        badInputPrompt();
                    }
                    else{
                        dex.sortDex(choice);
                    }
                    break;
            // Filter database
            case 3: choice = filterPrompt();
                    if(choice > 12 || choice < 1){
                        badInputPrompt();
                    }
                    else{
                        dex.filterDex(choice);
                    }
                    break;
            // Search database
            case 4: choice = searchPrompt();
                    if(choice > 2 || choice < 1){
                        badInputPrompt();
                    }
                    else{
                        dex.searchDex(choice);
                    }
                    break;
            // Return to original database
            case 5: dex.revertDex();
                    break;
            // Done finish program execution.
            case 6: done = true;
                    break;
            // invalid input
            default: cin.clear();
                     cin.ignore(numeric_limits<streamsize>::max(), '\n');
                     badInputPrompt();
                     break;
        }
    }
}

// Print main prompt
void mainPrompt()
{
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to the  \n");
    fprintf(stdout, "function you would like to perform:           \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1. Print Current Database                     \n");
    fprintf(stdout, "2. Sort the Database                          \n");
    fprintf(stdout, "3. Filter the Database                        \n");
    fprintf(stdout, "4. Search the Database                        \n");
    fprintf(stdout, "5. Return to default Database                 \n");
    fprintf(stdout, "6. Done                                       \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");
}

int sortPrompt()
{
    int choice;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to the  \n");
    fprintf(stdout, "attribute to sort by:                         \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1.  National Dex Number                       \n");
    fprintf(stdout, "2.  Name                                      \n");
    fprintf(stdout, "3.  Primary Type                              \n");
    fprintf(stdout, "4.  Secondary Type                            \n");
    fprintf(stdout, "5.  Ability 1                                 \n");
    fprintf(stdout, "6.  Ability 2                                 \n");
    fprintf(stdout, "7.  Hidden Ability                            \n");
    fprintf(stdout, "8.  Height                                    \n");
    fprintf(stdout, "9.  Weight                                    \n");
    fprintf(stdout, "10. HP                                        \n");
    fprintf(stdout, "11. Attack                                    \n");
    fprintf(stdout, "12. Defense                                   \n");
    fprintf(stdout, "13. Special Attack                            \n");
    fprintf(stdout, "14. Special Defense                           \n");
    fprintf(stdout, "15. Speed                                     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    return choice;
}

int filterPrompt()
{
    int choice;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to the  \n");
    fprintf(stdout, "attribute to filter by:                       \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1.  Primary Type                              \n");
    fprintf(stdout, "2.  Secondary Type                            \n");
    fprintf(stdout, "3.  Ability                                   \n");
    fprintf(stdout, "4.  Hidden Ability                            \n");
    fprintf(stdout, "5.  Height                                    \n");
    fprintf(stdout, "6.  Weight                                    \n");
    fprintf(stdout, "7.  HP                                        \n");
    fprintf(stdout, "8.  Attack                                    \n");
    fprintf(stdout, "9.  Defense                                   \n");
    fprintf(stdout, "10. Special Attack                            \n");
    fprintf(stdout, "11. Special Defense                           \n");
    fprintf(stdout, "12. Speed                                     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    return choice;
}

int searchPrompt()
{
    int choice;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please enter the number corresponding to the  \n");
    fprintf(stdout, "attribute to search by:                       \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "1.  Number                                    \n");
    fprintf(stdout, "2.  Name                                      \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Choice: ");

    cin >> choice;

    return choice;
}

// Check for usage requirements
void usageCheck(const int &argc, char *argv[])
{
    bool usage_error = false;

    if(argc > 1){
        usage_error = true;
    }

    if(usage_error){

        printUsage();
        exit(1);
    }
}

// Print usage message
void printUsage()
{
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Usage: ./bin/pokedex                              \n");
}
