//-----------------------------------------------------------------------------
// File: pokemon.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/18/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Function definitions for Pokemon class
//-----------------------------------------------------------------------------

#include <iostream>
#include <cstdio>
#include <cstring>

#include "pokemon.hpp"

using namespace std;

// ----------------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------------

Pokemon::Pokemon()
{
    // Set defaults
    number = -1;
    name = "DEFAULT";
    type1 = "DEFAULT";
    type2 = "DEFAULT";
    ability1 = "DEFAULT";
    ability2 = "DEFAULT";
    h_ability = "DEFAULT";
    height = -1;
    weight = -1;
    hp = -1;
    attack = -1;
    defense = -1;
    sp_attack = -1;
    sp_defense = -1;
    speed = -1;
}

Pokemon::Pokemon(const Pokemon &p)
{
    number = p.getNum();
    name = p.getName();
    type1 = p.getType(0);
    type2 = p.getType(1);
    ability1 = p.getAbility(0);
    ability2 = p.getAbility(1);
    h_ability = p.getAbility(2);
    height = p.getHeight();
    weight = p.getWeight();
    hp = p.getStat(0);
    attack = p.getStat(1);
    defense = p.getStat(2);
    sp_attack = p.getStat(3);
    sp_defense = p.getStat(4);
    speed = p.getStat(5);
}

// ----------------------------------------------------------------------------
// Accessors
// ----------------------------------------------------------------------------

string Pokemon::getInfo() const
{
    char buf[500];

    sprintf(buf, "%d: %s\n", number, name.c_str());
    sprintf(buf + strlen(buf), "   Type(s): %s, %s\n", type1.c_str(), type2.c_str());
    sprintf(buf + strlen(buf), "   Abilities: %s, %s\n", ability1.c_str(), ability2.c_str());
    sprintf(buf + strlen(buf), "   Hidden Ability: %s\n", h_ability.c_str());
    sprintf(buf + strlen(buf), "   Height: %f meters\n", height);
    sprintf(buf + strlen(buf), "   Weight: %f lbs\n", weight);
    sprintf(buf + strlen(buf), "   HP: %d\n", hp);
    sprintf(buf + strlen(buf), "   Attack: %d\n", attack);
    sprintf(buf + strlen(buf), "   Defense: %d\n", defense);
    sprintf(buf + strlen(buf), "   Special Attack: %d\n", sp_attack);
    sprintf(buf + strlen(buf), "   Special Defense: %d\n", sp_defense);
    sprintf(buf + strlen(buf), "   Speed: %d\n", speed);

    string info(buf);

    return info;
}

int Pokemon::getNum() const
{
    return number;
}

string Pokemon::getName() const
{
    return name;
}

string Pokemon::getType(const int &num) const
{
    if(num == 0){

        return type1;
    }
    else if(num == 1){

        return type2;
    }
    else{

        fprintf(stdout, "Incorrect Argument => Function: getType(const int& num)\n");
        fprintf(stdout, "Argument expected as 0 or 1.\n");
        exit(1);
    }
}

string Pokemon::getAbility(const int &num) const
{
    if(num == 0){

        return ability1;
    }
    else if(num == 1){

        return ability2;
    }
    else if(num == 2){

        return h_ability;
    }
    else{

        fprintf(stdout, "Incorrect Argument => Function: getAbility(const int& num)\n");
        fprintf(stdout, "Argument expected as 0 - 2.\n");
        exit(1);
    }
}

double Pokemon::getHeight() const
{
    return height;
}

double Pokemon::getWeight() const
{
    return weight;
}

int Pokemon::getStat(const int &num) const
{
    switch(num) {

        case 0: return hp;
        case 1: return attack;
        case 2: return defense;
        case 3: return sp_attack;
        case 4: return sp_defense;
        case 5: return speed;

        default:
            fprintf(stdout, "Incorrect Argument => Function: getStat(const int& num)\n");
            fprintf(stdout, "Argument expected as 0 - 5.\n");
            exit(1);
    }
}

// ----------------------------------------------------------------------------
// Modifiers
// ----------------------------------------------------------------------------

void Pokemon::setNum(const int& new_num)
{
    number = new_num;
}

void Pokemon::setName(const string &new_name)
{
    name = new_name;
}

void Pokemon::setType(const int &num, const string &new_type)
{
    if(num == 0){

        type1 = new_type;
    }
    else if(num == 1){

        type2 = new_type;
    }
    else{

        fprintf(stdout, "Incorrect Argument => Function: setType(const int& num, const string &new_type)\n");
        fprintf(stdout, "First argument expected as 0 or 1.\n");
        exit(1);
    }
}

void Pokemon::setAbility(const int &num, const string &new_ability)
{
    if(num == 0){

        ability1 = new_ability;
    }
    else if(num == 1){

        ability2 = new_ability;
    }
    else if(num == 2){

        h_ability = new_ability;
    }
    else{

        fprintf(stdout, "Incorrect Argument => Function: setAbility(const int& num, const string &new_ability)\n");
        fprintf(stdout, "First argument expected as 0 - 2.\n");
        exit(1);
    }
}

void Pokemon::setHeight(const double &new_height)
{
    height = new_height;
}

void Pokemon::setWeight(const double &new_weight)
{
    weight = new_weight;
}

void Pokemon::setStat(const int &num, const int& new_stat)
{
    switch(num) {

        case 0: hp = new_stat; break;
        case 1: attack = new_stat; break;
        case 2: defense = new_stat; break;
        case 3: sp_attack = new_stat; break;
        case 4: sp_defense = new_stat; break;
        case 5: speed = new_stat; break;

        default:
            fprintf(stdout, "Incorrect Argument => Function: getStat(const int& num)\n");
            fprintf(stdout, "Argument expected as 0 - 5.\n");
            exit(1);
    }
}

// ----------------------------------------------------------------------------
// Utility Functions
// ----------------------------------------------------------------------------

void Pokemon::printData()
{
    fprintf(stdout, "%d: %s\n", number, name.c_str());
    fprintf(stdout, "   Type(s): %s, %s\n", type1.c_str(), type2.c_str());
    fprintf(stdout, "   Abilities: %s, %s\n", ability1.c_str(), ability2.c_str());
    fprintf(stdout, "   Hidden Ability: %s\n", h_ability.c_str());
    fprintf(stdout, "   Height: %f meters\n", height);
    fprintf(stdout, "   Weight: %f lbs\n", weight);
    fprintf(stdout, "   HP: %d\n", hp);
    fprintf(stdout, "   Attack: %d\n", attack);
    fprintf(stdout, "   Defense: %d\n", defense);
    fprintf(stdout, "   Special Attack: %d\n", sp_attack);
    fprintf(stdout, "   Special Defense: %d\n", sp_defense);
    fprintf(stdout, "   Speed: %d\n", speed);
}
