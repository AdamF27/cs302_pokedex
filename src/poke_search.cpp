//-----------------------------------------------------------------------------
// File: poke_search.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/30/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Function definitions for poke searching
//-----------------------------------------------------------------------------

#include <cstdio>
#include <iostream>

#include "poke_search.hpp"
#include "utility.hpp"

using namespace std;

// Searching functions --------------------------------------------------------

void searchNum(vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    int num = numPrompt();

    if(num < 1){

        badInputPrompt();
        return;
    }

    for(unsigned int i = 0; i < mod.size(); i++){

        if(mod[i]->getNum() == num){

            newv.push_back(mod[i]);
            break;
        }
    }

    mod = newv;
}

void searchNum(vector<Pokemon*> &mod, const int& num)
{
    vector<Pokemon*> newv;

    if(num < 1){

        mod = newv;
        return;
    }

    for(unsigned int i = 0; i < mod.size(); i++){

        if(mod[i]->getNum() == num){

            newv.push_back(mod[i]);
            break;
        }
    }

    mod = newv;
}

void searchName(vector<Pokemon*> &mod)
{
    vector<Pokemon*> newv;

    string name = namePrompt();

    for(unsigned int i = 0; i < mod.size(); i++){

        if(mod[i]->getName() == name){

            newv.push_back(mod[i]);
            break;
        }
    }

    mod = newv;
}

void searchName(vector<Pokemon*> &mod, const string& name)
{
    vector<Pokemon*> newv;

    for(unsigned int i = 0; i < mod.size(); i++){

        if(mod[i]->getName() == name){

            newv.push_back(mod[i]);
            break;
        }
    }

    mod = newv;
}

// Necessary prompts ----------------------------------------------------------

int numPrompt()
{
    int num;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please type the national pokedex number you   \n");
    fprintf(stdout, "would like to search for.                     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Nat'l Dex Number: ");

    cin >> num;

    return num;
}

string namePrompt()
{
    string name;

    fprintf(stdout, "                                              \n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "--- CS302 Final Project - Pokedex Emulator ---\n");
    fprintf(stdout, "----------------------------------------------\n");
    fprintf(stdout, "     Authors: Adam Foshie & Kelsey Kelley     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "==============================================\n");
    fprintf(stdout, "Please type the name of the pokemon you would \n");
    fprintf(stdout, "like to search for.                     \n");
    fprintf(stdout, "                                              \n");
    fprintf(stdout, "Name: ");

    cin >> name;

    return name;
}
