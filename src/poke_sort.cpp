//-----------------------------------------------------------------------------
// File: poke_sort.cpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/30/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Function definitions for poke sorting
//-----------------------------------------------------------------------------

#include "poke_sort.hpp"
#include "utility.hpp"

// Sorting functions
// These all use quick sort and are mostly the same code repeated.
// TODO: make this file shorter by reusing code. copy paste for now is quicker

void sortNum(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getNum() < piv->getNum()){

            i++;
        }
        while(dex[j]->getNum() > piv->getNum()){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortNum(dex, i, R);
            if(j > L)
                sortNum(dex, L, j);
            return;
        }
    }
}

void sortName(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getName() < piv->getName()){

            i++;
        }
        while(dex[j]->getName() > piv->getName()){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortName(dex, i, R);
            if(j > L)
                sortName(dex, L, j);
            return;
        }
    }
}

void sortType1(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getType(0) < piv->getType(0)){

            i++;
        }
        while(dex[j]->getType(0) > piv->getType(0)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortType1(dex, i, R);
            if(j > L)
                sortType1(dex, L, j);
            return;
        }
    }
}

void sortType2(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getType(1) < piv->getType(1)){

            i++;
        }
        while(dex[j]->getType(1) > piv->getType(1)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortType2(dex, i, R);
            if(j > L)
                sortType2(dex, L, j);
            return;
        }
    }
}

void sortAbility1(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getAbility(0) < piv->getAbility(0)){

            i++;
        }
        while(dex[j]->getAbility(0) > piv->getAbility(0)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortAbility1(dex, i, R);
            if(j > L)
                sortAbility1(dex, L, j);
            return;
        }
    }
}

void sortAbility2(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getAbility(1) < piv->getAbility(1)){

            i++;
        }
        while(dex[j]->getAbility(1) > piv->getAbility(1)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortAbility2(dex, i, R);
            if(j > L)
                sortAbility2(dex, L, j);
            return;
        }
    }
}

void sortHiddenAbility(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getAbility(2) < piv->getAbility(2)){

            i++;
        }
        while(dex[j]->getAbility(2) > piv->getAbility(2)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortHiddenAbility(dex, i, R);
            if(j > L)
                sortHiddenAbility(dex, L, j);
            return;
        }
    }
}

void sortHeight(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getHeight() > piv->getHeight()){

            i++;
        }
        while(dex[j]->getHeight() < piv->getHeight()){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortHeight(dex, i, R);
            if(j > L)
                sortHeight(dex, L, j);
            return;
        }
    }
}

void sortWeight(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getWeight() > piv->getWeight()){

            i++;
        }
        while(dex[j]->getWeight() < piv->getWeight()){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortWeight(dex, i, R);
            if(j > L)
                sortWeight(dex, L, j);
            return;
        }
    }
}

void sortHP(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getStat(0) > piv->getStat(0)){

            i++;
        }
        while(dex[j]->getStat(0) < piv->getStat(0)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortHP(dex, i, R);
            if(j > L)
                sortHP(dex, L, j);
            return;
        }
    }
}

void sortAtk(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getStat(1) > piv->getStat(1)){

            i++;
        }
        while(dex[j]->getStat(1) < piv->getStat(1)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortAtk(dex, i, R);
            if(j > L)
                sortAtk(dex, L, j);
            return;
        }
    }
}

void sortDef(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getStat(2) > piv->getStat(2)){

            i++;
        }
        while(dex[j]->getStat(2) < piv->getStat(2)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortDef(dex, i, R);
            if(j > L)
                sortDef(dex, L, j);
            return;
        }
    }
}

void sortSpAtk(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getStat(3) > piv->getStat(3)){

            i++;
        }
        while(dex[j]->getStat(3) < piv->getStat(3)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortSpAtk(dex, i, R);
            if(j > L)
                sortSpAtk(dex, L, j);
            return;
        }
    }
}

void sortSpDef(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getStat(4) > piv->getStat(4)){

            i++;
        }
        while(dex[j]->getStat(4) < piv->getStat(4)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortSpDef(dex, i, R);
            if(j > L)
                sortSpDef(dex, L, j);
            return;
        }
    }
}

void sortSpeed(vector<Pokemon*> &dex, const int &L, const int &R)
{
    // Iteration variables
    int i = L;
    int j = R;

    // Pivot variables
    int mid;
    Pokemon* piv;

    // Set pivot as middle element
    mid = L + (R - L) / 2;
    piv = dex[mid];

    while (i < R || j > L){

        while(dex[i]->getStat(5) > piv->getStat(5)){

            i++;
        }
        while(dex[j]->getStat(5) < piv->getStat(5)){

            j--;
        }

        // If base case just swap
        if(i <= j) {

            // Swap elements
            swap_elem(dex, i, j);
            i++;
            j--;
        }
        // Recursive call to quick sort
        else{

            if(i < R)
                sortSpeed(dex, i, R);
            if(j > L)
                sortSpeed(dex, L, j);
            return;
        }
    }
}
