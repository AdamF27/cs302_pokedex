Kelsey Kelley and Adam Foshie

Graphical Pokedex

Project Summary:
Create an interactive and graphical Pokedex, associated with the game and anime Pokemon
A database of the Pokemon will be used in the program to perform several functions within
it, such as, sorting, filtering, and searching. 

Time Log:
  3/20/2019 - Created first 50 entries into database - 1 hour
  3/24/2019 - Extended database to 100 entries - 1 hour
  3/24/2019 - Team meeting to determine individual assignments - 1 hour
  3/27/2019 - index up to 151 added, and started working with qt

Repository link:
  https://bitbucket.org/AdamF27/pokedex/src/master/

Overall Rubric:
  10 - Create text based pokedex with all required information
  5 - qt tutorials to understand how to integrate back-end into front-end
  10 - display database using sprites
  5 - connect back-end functionality to buttons
  10 - creating menus associated with the sorting, filtering and searching functions

Rubric for Week of 3/25 - 3/29
  20 - qt tutorials 
  10 - create indexes 50-150 and add them into the database
  10 - design and start draft layout for pokedex

Summary of work so far and next steps planned:
  So far I have been updating the database of Pokemon and have fully updated to 
  index number 151. I have also began learning some of the ins and outs of qt
  in order to better understand how the Graphical Interface portion will work.
  Adam and I have also had a meeting to discuss the direction we want to take the project,
  our big picture goal and we have also discussed our progress on the project. 
  My next steps for the week are to continue updating the database and to start going 
  through some qt tutorials. 
 
  
