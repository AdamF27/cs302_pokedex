#include <gtk/gtk.h>

static void callback( GtkWidget *widget,
                      gpointer   data )
{
    GtkBuilder *builder;
    GtkWidget *window;
    GError *error = NULL;

    builder = gtk_builder_new();
    if( ! gtk_builder_add_from_file( builder, "src/search_pressed.glade", &error ) )
    {
       g_warning("%s", error->message);
       g_free(error);
    }

    window = GTK_WIDGET(gtk_builder_get_object(builder, "searchpressed"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_show(window);
}


int main(int argc, char **argv){

	GtkBuilder *builder;
	GtkWidget *window;
	GError *error = NULL;
	GtkButton *search;

	/* init GTK+ */
	gtk_init( &argc, &argv );

	/* Create a new GtkBuilder object */
	builder = gtk_builder_new();

	/*Load UI from file*/
	if( ! gtk_builder_add_from_file( builder, "src/poke_viz.glade", &error ) )
    {
        g_warning( "%s", error->message );
        g_free( error );
        return( 1 );
    }

    /* Get main window pointer from UI */
    window = GTK_WIDGET( gtk_builder_get_object( builder, "pokedex" ) );

    search = GTK_BUTTON(gtk_builder_get_object(builder, "search"));

    /* Connect signals */
    gtk_builder_connect_signals( builder, NULL );

    /* Destroy builder, since we don't need it anymore */
    g_object_unref( G_OBJECT( builder ) );


    g_signal_connect (search, "clicked", G_CALLBACK (callback), (gpointer) "Search");

    /* Show window. All other widgets are automatically shown by GtkBuilder */
    gtk_widget_show( window );

    /* Start main loop */
    gtk_main();

    return( 0 );
}
