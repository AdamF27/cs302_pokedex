# CS302 Final Project - Graphical Pokedex

-----

[TOC]

-----

## Overview

This work was a final project for CS302 at The University of Tennessee - 
Knoxville. It was created by Adam Foshie and Kelsey Kelley. The objective was
to create an interactive and graphical Pokedex from the game and anime series 
Pokemon. This is accomplished by parsing information from a database file of
Pokemon and allowing several functions to be performed on the list such as
searching, filtering, and sorting.

## Required Software Installations

### GTK3

The GUI for this is written using GTK3. Due to this, it will be necessary to
install GTK on your machine to compile the code. To install GTK3+ on Debian
OS run:

`sudo apt-get install libgtk-3-dev`

The same package should be available to other operating systems. It has been
tested on Ubuntu and Mac.

### pkg-config

The compile commands for this use pkg-config, so it is also required and can
be installed with the following command:

`sudo apt-get install pkg-config`

After installing the required packages and then compiling with the pkg-config on Mac, 
an error occurs stating it cannot find the Package libffi and it needs to be 
added to the PKG_CONFIG_PATH environment variable, to remedy this situation 
run the command:

`export PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig/"` 

## Database File Format

`database/pokemon.txt` will hold the information about all of the pokemon we are
going to display in the pokedex. The database will be limited to generation 3 so
that there are enough pokemon to ensure proper operation but not so much that
we spend too much time just creating the database. The database file will have
the following format:

-------
    Number  
        Name  
        Type1  
        Type2  
        Ability1  
        Ability2  
        Hidden Ability  
        Height (m)  
        Weight (lbs)  
        HP  
        Attack  
        Defense  
        Sp. Atk  
        Sp. Def  
        Speed
------

If this format is not followed by each entry, we cannot guarantee anything
about the operation of the Pokedex Emulator.

## Sprites

The folder `sprites/` contains a list of `.png` files that contian the sprite for each pokemon. These are named after the national pokedex number of their corresponding pokemon.

## Compilation

To compile the program simply call `make` in the top level of the code repository.

This will create two executables: `bin/pokedex` and `bin/pokedex_text`.

`bin/pokedex` is the graphical pokedex emulator. `bin/pokedex_text` is the text based emulator that was used to ensure proper operation of the supported functions.

To remove all executables and object files simply call `make clean`.

## Usage

### Graphical Pokedex

To run the graphical pokedex, run `./bin/pokedex`. It is important to run it from the
top level and not within the `bin/` folder as there are links to the sprites folder
and glade files for the GUI that it will not be able to find otherwise.

![window_example](images/window_example.png)

Above is an image displaying what the window should look like after running the executable.

Each of the sections of this window are shown and described below.

#### Search Button

The first button on the left is the search button. The search functionality is intended
for cases where you know the name or national pokedex number of a pokemon you would like
to know more information about. This button will open a window with two more buttons to
determine whether you want to search for a name or a number. There is also a button to go
back on any additional window pop-ups in case you make a mistake click.

When you click on either the name or number button a new window will appear that has a
text entry box and a search button. To complete your search simply enter the name or number
you wish to search for and click "Search!"

#### Sort Button

The next button below is for sorting. This button and all the remaining buttons in this
column act very similarly to the search button; the intended use is the main difference.
For the sort button, you can choose to sort your current pokedex (displayed on the right)
by any attribute.

When the sort window appears simply click the desired attribute to sort by.

#### Filter Button (NOT YET IMPLEMENTED GRAPHICALLY)

The filter button is intended to weed out any pokemon in your pokedex that do not meet
some requirement. This is achieved by filtering out any pokemon that do not have a
particular attribute you specify. Several pop-up windows allow you to pick the attribute in
question you want to filter by then in what way you want to filter it. For example, for
numeric based stats, you can filter out pokemon that are less than, greater than, or
not equal to your desired value. For string based attributes, you filter out any pokemon that
do not match your desired string entry.

#### Revert Button

One main benefit of this Pokedex is that it is persistent. This means that any filtering or sorting
that you do can be continually added onto until you are left with what you desire. This brings about
a necessity for a way to return to the original full database. That is what this button does.
It will return you to the top of the pokedex and your list will be back to full.

#### Information Display Box

Here is where the information of the currently selected Pokemon is displayed. The currently 
selected pokemon is the middle sprite in the sprite display area.

#### Movement Buttons

This column of buttons is how you navigate through your current pokedex results. The three top
buttons are for moving "Up" the pokedex list on the right and the botton three buttons move "Down".
The three buttons are for moving at different rates. The buttons closest to the center move by 1 "Up"
or "Down" and as they move away from the center they move by 10 and by 100 in their respective direction.

#### Sprite Display Box

This is where the graphical pokedex shines. Behold, actual sprites for the pokemon you want to
check out. A very simple addition, yes, but one that makes all the difference. Here is where those
sprites get to be displayed. The middle sprite is considered your currently selected pokemon.
Any sprite that shows a question mark in a white circle is considered "out of bounds" in your pokedex.
That is either before the first entry or after the last entry. It is possible to search for
something that doesn't exist or filter out pokemon until none remain. If this happens, you
will be left with all three sprites as this white circle question mark sprite. To go back to 
the original pokedex you simple need to click the "Revert" button described before.

#### Minimize and Close Buttons

The last two buttons are the familiar minimize and close buttons. These do what they
should do: minimize and close the window.

### Text Based Pokedex

The text based pokedex can be ran by calling `./bin/pokedex_text`. This pokedex was created as a baseline for
creating the back end functions of the pokedex. Usage details are not really required here because the text
based pokedex prompts you specifically for what you desire each step of the way. All functions work on it though,
so if you do not want the flashy nature and windows of the graphical pokedex, give this a go.

## Known Bugs or Limitations

One over-arching limitation that is not really a bug is the sorting algorithm that was used. We use
quick sort as the sorting algorithm and that means it is not stable. When doing multiple filtering and
sorting operations to find what pokemon you are looking for, it is likely desirable for this to be
stable, but changing this to merge sort has not yet been completed. So, keep this in mind when
operating either the graphical or text based pokedex.

### Graphical Pokedex

#### Filter Functionality

The filter functionality of the Pokedex is still not implemented in the GUI. This is
not due to the underlying functions not working; It is simply because of the amount of buttons
and widgets that have to be connected to it. This will be updated soon, but might not be fully finished until after the official due date of this project.

#### Entry box issues

For entry boxes that request numbers, we found no way to make the entry boxes only accept
numbers, so to get around this, we simply run a check to see if the entry contains
alpha characters. If it does, it searches for the entry -1 which does not exist. This will
return an empty pokedex list. To search for numbers make sure to enter digit values not their
spelled out counterpart.

Also, just a note when searching for string based attributes. The capitalization matters.
That is if you want to search for Charmander, you must use a capital "C" or it will not
find Charmander in the pokedex.

#### Pressing buttons on a non-primary window

At the moment it is possible to minimize or simply click around a popup window to click buttons
on the primary window of the pokedex. This can lead to duplicate windows opening and can lead to 
issues later on in operation due to windows being opened twice but closed once or any similar
circumstance. We think it is possible to disable any interaction with non-focused windows, but for
now this is a "Bug".

### Text Based Pokedex

No known bugs at this time.

## Time Log

### 3/16/2019

AF: Set up repository file structure - 1 hour

### 3/18/2019

AF: Decided on database file format, added sprites, and defined pokemon class
variables and member functions - 3 hours

### 3/20/2019

KK: Created first 50 database entries - 1 hour

### 3/23/2019

AF: Restructured files and created setup main for pokedex operations. Also created base makefile - 1 hour

### 3/24/2019

AF/KK: Team Meeting to determine individual assignments - 1 hour

KK: Created database entries up to 100 - 1 hour

### 3/25/2019

AF/KK: Time in lab completing challenge 08 rubric creation. - 1.5 hours

### 3/27/2019

KK: Created database entries up to 151 to finish Gen 1 - 1 hour
KK: Started doing tutorials in qt - 1.5 hours

### 3/30/2019

AF: Created Pokedex class and set up cpp files for functions. - 2 hours

### 3/31/2019

KK: Database up to 200, started working on outline of pokedex in gtk - 2 hours

### 4/2/2019

AF: Sorting implemented - 2 hours

### 4/6/2019

AF: Started implementation of filtering and searching - 2 hours

### 4/7/2019

KK: Database up to 251, Gen 2 complete - 1 hour

### 4/12/2019

KK: gtk layout and learning - 1 hour

### 4/13/2019

KK: database up to 212 - 1 hour

### 4/15/2019

KK: databse up to 278 - 1 hour

### 4/18/2019

KK: database is complete, Gen 3 to index 386 - 2 hours
AF: Finish filtering and searching functions - 5 hours

### 4/19/2019

AF: Debug and test back end functions - 1 hour
KK: GUI layout - 1.5 hours

### 4/20/2019

KK: Experimental button pressing window setup - 1.5 hours

### 4/21/2019

AF: GUI pokedex scrolling with info now implemented - 5 hours

### 4/23/2019

KK: glade files added for the use of the functions within the pokedex - 2.5 hours
AF: GUI now supports Sorting functions - 3 hours

### 4/24/2019

KK: Glade files for separate function calls added within the main glade file - 2 hours

### 4/26/2019

AF: Searching now supported in GUI - 3 hours

### 4/27/2019

AF: Created User Manual - 2 hours

## Team Meetings

The meetings listed here are in addition to the regular meetings we had in lab sessions or otherwise for
no specific reasons.

### Project Focus Meeting

This meeting was our first big meeting to discuss what this project was going to be about.
This of course ended up as the pokedex that was detailed above.

### Individual Tasks Breakdown Meeting

The individual tasks meeting was to decide how our time would be broken up for first half or so of the
project. This ended up being split such that Adam focused on a text based pokedex that had
all of the backend functionality of the pokedex and Kelsey focused on creating the database
of pokemon and learning about Qt and GTK to determine which would be best for us.

### Visualization Software Meeting

This is the meeting where we made a hard decision on the visualization tool. We ended up choosing
to go with GTK3 because of its easy integration with our makefile setup and a small amount of
experience using GTK3.

### Progress Meeting

The progress meeting here was to update our tasks essentially. This is when we both
started to merge together to focus finalizing the functionality of the text based pokedex
and determining how the GUI should look and operate in the end.

### GTK3 Update Meeting

This meeting was to work through difficulties that were encountered with GTK3. This meeting
helped us move over to using Glade. Glade is a GTK3 tool that helps you build visualized windows
with Widgets in a more intuitive way than with straight code. This was what allowed us to push
past our struggles and end up where we are with the GUI.
