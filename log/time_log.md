# CS302 Final Project Time Log

## 3/16/2019

AF: Set up repository file structure - 1 hour

## 3/18/2019

AF: Decided on database file format, added sprites, and defined pokemon class
variables and member functions - 3 hours

## 3/20/2019

KK: Created first 50 database entries - 1 hour

## 3/23/2019

AF: Restructured files and created setup main for pokedex operations. Also created base makefile - 1 hour

## 3/24/2019

AF/KK: Team Meeting to determine individual assignments - 1 hour

KK: Created database entries up to 100 - 1 hour

## 3/25/2019

AF/KK: Time in lab completing challenge 08 rubric creation. - 1.5 hours

## 3/27/2019

KK: Created database entries up to 151 to finish Gen 1 - 1 hour
KK: Started doing tutorials in qt - 1.5 hours

## 3/30/2019

AF: Created Pokedex class and set up cpp files for functions. - 2 hours

## 3/31/2019

KK: Database up to 200, started working on outline of pokedex in gtk - 2 hours

## 4/2/2019

AF: Sorting implemented - 2 hours

## 4/6/2019

AF: Started implementation of filtering and searching - 2 hours

## 4/7/2019

KK: Database up to 251, Gen 2 complete - 1 hour

## 4/12/2019

KK: gtk layout and learning - 1 hour

## 4/13/2019

KK: database up to 212 - 1 hour

## 4/15/2019

KK: databse up to 278 - 1 hour

## 4/18/2019

KK: database is complete, Gen 3 to index 386 - 2 hours
AF: Finish filtering and searching functions - 5 hours

## 4/19/2019

AF: Debug and test back end functions - 1 hour
KK: GUI layout - 1.5 hours

## 4/20/2019

KK: Experimental button pressing window setup - 1.5 hours

## 4/21/2019

AF: GUI pokedex scrolling with info now implemented - 5 hours

## 4/23/2019

KK: glade files added for the use of the functions within the pokedex - 2.5 hours
AF: GUI now supports Sorting functions - 3 hours

# 4/24/2019

KK: Glade files for separate function calls added within the main glade file - 2 hours

# 4/26/2019

AF: Searching now supported in GUI - 3 hours

# 4/27/2019

AF: Created User Manual - 2 hours

