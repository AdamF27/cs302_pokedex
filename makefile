CXX=		g++
CXXFLAGS=	`pkg-config --libs --cflags gtk+-3.0` -g -Wall -std=gnu++11 -I$(INCLUDE)
GTKFLAGS=   `pkg-config --libs --cflags gtk+-3.0`
SHELL=		bash
PROGRAMS=	bin/pokedex bin/pokedex_text
OBJECTS_MAIN=    obj/pokemon.o obj/poke_sort.o obj/poke_filter.o obj/poke_search.o obj/pokedex.o obj/utility.o obj/buttons.o obj/main.o
OBJECTS_TEXT=    obj/pokemon.o obj/poke_sort.o obj/poke_filter.o obj/poke_search.o obj/pokedex.o obj/utility.o obj/text.o
INCLUDE=    include/
OBJ=        obj/
SRC=        src/
BIN=        bin/


all:	$(PROGRAMS)

$(OBJ)pokemon.o:	$(SRC)pokemon.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)poke_sort.o:	$(SRC)poke_sort.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)poke_filter.o:	$(SRC)poke_filter.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)poke_search.o:	$(SRC)poke_search.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)pokedex.o:	$(SRC)pokedex.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)utility.o:	$(SRC)utility.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)buttons.o:	$(SRC)buttons.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)text.o:	$(SRC)text.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(OBJ)main.o:	  $(SRC)main.cpp | $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ -c $^

$(BIN)pokedex_text:    $(OBJECTS_TEXT) | $(BIN)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(BIN)pokedex:    $(OBJECTS_MAIN) | $(BIN)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(GTKFLAGS)


$(BIN):
	mkdir -p $(BIN)

$(OBJ):
	mkdir -p $(OBJ)

.PHONY:    clean

clean:
	rm -f $(PROGRAMS)
	rm -f $(OBJECTS_TEXT)
	rm -f $(OBJECTS_MAIN)
