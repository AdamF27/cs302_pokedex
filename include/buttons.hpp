//-----------------------------------------------------------------------------
// File: buttons.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 4/22/2019
// Project: CS302 Final Project - Pokedex
//
// Description: GUI button functions
//-----------------------------------------------------------------------------

#pragma once

#include <gtk/gtk.h>
#include <cstdio>
#include <iostream>

using namespace std;

// GUI Button Functions -------------------------------------------------------

// Main buttons

void up1_pressed(GtkWidget *widget, gpointer data);

void up10_pressed(GtkWidget *widget, gpointer data);

void up100_pressed(GtkWidget *widget, gpointer data);

void down1_pressed(GtkWidget *widget, gpointer data);

void down10_pressed(GtkWidget *widget, gpointer data);

void down100_pressed(GtkWidget *widget, gpointer data);

void search_pressed(GtkWidget *widget, gpointer data);

void sort_pressed(GtkWidget *widget, gpointer data);

void filter_pressed(GtkWidget *widget, gpointer data);

void revert_pressed(GtkWidget *widget, gpointer data);

// Sort Buttons

void sort_num_pressed(GtkWidget *widget, gpointer data);

void sort_name_pressed(GtkWidget *widget, gpointer data);

void sort_ptype_pressed(GtkWidget *widget, gpointer data);

void sort_stype_pressed(GtkWidget *widget, gpointer data);

void sort_ability1_pressed(GtkWidget *widget, gpointer data);

void sort_ability2_pressed(GtkWidget *widget, gpointer data);

void sort_hability_pressed(GtkWidget *widget, gpointer data);

void sort_height_pressed(GtkWidget *widget, gpointer data);

void sort_weight_pressed(GtkWidget *widget, gpointer data);

void sort_HP_pressed(GtkWidget *widget, gpointer data);

void sort_ATK_pressed(GtkWidget *widget, gpointer data);

void sort_DEF_pressed(GtkWidget *widget, gpointer data);

void sort_SPATK_pressed(GtkWidget *widget, gpointer data);

void sort_SPDEF_pressed(GtkWidget *widget, gpointer data);

void sort_SPEED_pressed(GtkWidget *widget, gpointer data);

void sort_back_pressed(GtkWidget *widget, gpointer data);

// Search Buttons

void search_name_pressed(GtkWidget *widget, gpointer data);

void search_num_pressed(GtkWidget *widget, gpointer data);

void search_back_pressed(GtkWidget *widget, gpointer data);

void search_name_search_pressed(GtkWidget *widget, gpointer data);

void search_name_back_pressed(GtkWidget *widget, gpointer data);

void search_num_search_pressed(GtkWidget *widget, gpointer data);

// Filter Buttons

void search_num_back_pressed(GtkWidget *widget, gpointer data);

void filter_ptype_pressed(GtkWidget *widget, gpointer data);

void filter_stype_pressed(GtkWidget *widget, gpointer data);

void filter_ability_pressed(GtkWidget *widget, gpointer data);

void filter_hability_pressed(GtkWidget *widget, gpointer data);

void filter_height_pressed(GtkWidget *widget, gpointer data);

void filter_weight_pressed(GtkWidget *widget, gpointer data);

void filter_back_pressed(GtkWidget *widget, gpointer data);

void filter_HP_pressed(GtkWidget *widget, gpointer data);

void filter_ATK_pressed(GtkWidget *widget, gpointer data);

void filter_DEF_pressed(GtkWidget *widget, gpointer data);

void filter_SPATK_pressed(GtkWidget *widget, gpointer data);

void filter_SPDEF_pressed(GtkWidget *widget, gpointer data);

void filter_SPEED_pressed(GtkWidget *widget, gpointer data);
