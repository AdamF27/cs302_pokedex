//-----------------------------------------------------------------------------
// File: pokedex.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/23/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Pokedex class declaration
//-----------------------------------------------------------------------------

#pragma once

#include <vector>

#include "pokemon.hpp"

using namespace std;

class Pokedex{

    public:

        // Constructors / Destructors
        Pokedex();
        Pokedex(const string& db_path);
        ~Pokedex();

        // Operator Overloading
        Pokedex& operator=(const Pokedex &dex);

        // Accessors
        vector<Pokemon*> getMasterDex() const;
        vector<Pokemon*> getModDex() const;

        // Modifiers

        // Utility Functions
        void printDex();
        void sortDex(const int& s_param);
        void filterDex(const int& f_param);
        void searchDex(const int& s_param);
        void searchDexName(const string& name);
        void searchDexNum(const int& num);
        void revertDex();


    private:

        vector<Pokemon*> master_dex; // Holds original database
        vector<Pokemon*> mod_dex;    // Holds filtered/sorted database

};
