//-----------------------------------------------------------------------------
// File: poke_sort.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/31/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Necessary sorting functions
//-----------------------------------------------------------------------------

#pragma once

#include <vector>

#include "pokemon.hpp"

using namespace std;

void sortNum(vector<Pokemon*> &dex, const int &L, const int &R);

void sortName(vector<Pokemon*> &dex, const int &L, const int &R);

void sortType1(vector<Pokemon*> &dex, const int &L, const int &R);

void sortType2(vector<Pokemon*> &dex, const int &L, const int &R);

void sortAbility1(vector<Pokemon*> &dex, const int &L, const int &R);

void sortAbility2(vector<Pokemon*> &dex, const int &L, const int &R);

void sortHiddenAbility(vector<Pokemon*> &dex, const int &L, const int &R);

void sortHeight(vector<Pokemon*> &dex, const int &L, const int &R);

void sortWeight(vector<Pokemon*> &dex, const int &L, const int &R);

void sortHP(vector<Pokemon*> &dex, const int &L, const int &R);

void sortAtk(vector<Pokemon*> &dex, const int &L, const int &R);

void sortDef(vector<Pokemon*> &dex, const int &L, const int &R);

void sortSpAtk(vector<Pokemon*> &dex, const int &L, const int &R);

void sortSpDef(vector<Pokemon*> &dex, const int &L, const int &R);

void sortSpeed(vector<Pokemon*> &dex, const int &L, const int &R);
