//-----------------------------------------------------------------------------
// File: utility.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/18/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Holds any utility functions that we may need
//-----------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

#include "pokemon.hpp"

using namespace std;

string leftTrim(const string &s);

void swap_elem(vector<Pokemon*> &dex, const int &i, const int &j);

void PressEnterToContinue();

void badInputPrompt();

bool isLetters(string s);
