//-----------------------------------------------------------------------------
// File: poke_filter.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/31/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Necessary filtering functions
//-----------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

#include "pokemon.hpp"

using namespace std;

// Filter Functions -----------------------------------------------------------

void filterType(const int &t, vector<Pokemon*> &mod);

void filterAbility(vector<Pokemon*> &mod);

void filterHAbility(vector<Pokemon*> &mod);

void filterHeight(vector<Pokemon*> &mod);

void filterWeight(vector<Pokemon*> &mod);

void filterStat(const int &s, vector<Pokemon*> &mod);

// Necessary Prompts ----------------------------------------------------------

string typePrompt();

string abilityPrompt();

void heightPrompt(int &comp, double &val);

void weightPrompt(int &comp, double &val);

void statPrompt(int &comp, int &val);
