//-----------------------------------------------------------------------------
// File: poke_search.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/31/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Necessary search functions
//-----------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

#include "pokemon.hpp"

using namespace std;

// Search Functions -----------------------------------------------------------

void searchNum(vector<Pokemon*> &mod);

void searchNum(vector<Pokemon*> &mod, const int& num);

void searchName(vector<Pokemon*> &mod);

void searchName(vector<Pokemon*> &mod, const string& name);

// Necessary Prompts ----------------------------------------------------------

int numPrompt();

string namePrompt();
