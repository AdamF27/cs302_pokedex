//-----------------------------------------------------------------------------
// File: pokemon.hpp
// Authors: Adam Foshie & Kelsey Kelley
// Date: 3/18/2019
// Project: CS302 Final Project - Pokedex
//
// Description: Header file for the pokemon class.
//-----------------------------------------------------------------------------

#pragma once

#include <string>

using namespace std;

class Pokemon {

    public:

        Pokemon();
        Pokemon(const Pokemon &p);

        // Accessors
        string getInfo() const;
        int    getNum() const;
        string getName() const;
        string getType(const int &num) const;
        string getAbility(const int &num) const;
        double getHeight() const;
        double getWeight() const;
        int    getStat(const int &num) const;

        // Modifiers
        void   setNum(const int &new_num);
        void   setName(const string &new_name);
        void   setType(const int &num, const string &new_type);
        void   setAbility(const int &num, const string &new_ability);
        void   setHeight(const double &new_height);
        void   setWeight(const double &new_weight);
        void   setStat(const int &num, const int& new_stat);

        // Utility Functions
        void printData();

    private:

        // Attributes
        int number;
        string name;
        string type1;
        string type2;
        string ability1;
        string ability2;
        string h_ability;
        double height;
        double weight;
        int hp;
        int attack;
        int defense;
        int sp_attack;
        int sp_defense;
        int speed;
};
